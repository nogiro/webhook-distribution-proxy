# concept

img src: https://docs.google.com/presentation/d/1cgHYXotN4Md3QEuZ9brmcQTzTtq_VqhtCPqJtqyRDls/edit?usp=sharing

## Webhooks in production

Webhook 機能は外部サービスから自身のサービスに、外部サービスのイベントを送る用途でよく使われます。

Webhooks are usually used to send events from an external service to own service.

![webhook in production](./img/concept_1.png)


## Webhooks in single localhost development

Webhook を利用するサービスを localhost で開発するときは、インターネット上のトンネリングサービスなど (e.g. [ngrok](https://ngrok.com/)) を用いて開発することがあります。

When develop a service releied webhooks in localhost, it's developed using a tunneling service (e.g. [ngrok](https://ngrok.com/)) on the internet.

![webhook in single localhost](./img/concept_2.png)


## Webhooks in multiple localhost development

外部サービスにおいて 1 つの Webhook のみしか登録できない場合や、Webhook 設定を変えたくない場合に、Webhook リクエストを複製することができると便利ではないか、というのがこのプロジェクトのコンセプトです。

In the following case, it is useful that webhook requests are duplicated to multiple host.

+ An only one webhook is registered in the external service.
+ You could not configure webhook settings in the external service.

![concept for multiple localhost development](./img/concept_3.png)
