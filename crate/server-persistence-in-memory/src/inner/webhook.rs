use wdp_server_core as wsc;
use wdp_struct as ws;

use std::collections::BTreeMap;
use std::sync::{Arc, RwLock};

#[derive(Default)]
pub struct WebhookContainer(Arc<RwLock<BTreeMap<ws::WebhookId, Webhook>>>);

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Webhook(ws::Webhook);

impl PartialOrd for Webhook {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.created_at.partial_cmp(&other.0.created_at)
    }
}

impl WebhookContainer {
    pub async fn list(
        &self,
        pagination: ws::Pagination,
    ) -> Result<ws::ListResponse<ws::Webhook>, wsc::Error> {
        let Ok(map) = self.0.read() else {
            return Err(wsc::Error::MutexPoisoned(ws::ModelKind::Webhook));
        };

        let (offset, limit) = pagination.pagination();
        let offset = offset.try_into().unwrap_or(0);
        let limit = limit.try_into().unwrap_or(10);

        let items = map
            .values()
            .skip(offset)
            .take(limit)
            .cloned()
            .map(|w| w.0)
            .collect();

        let count = map.len();

        let offset = offset.try_into().unwrap_or(0);
        let limit = limit.try_into().unwrap_or(10);
        let count = count.try_into().unwrap_or(0);

        Ok(ws::ListResponse {
            items,
            offset,
            limit,
            count,
        })
    }

    pub async fn add(
        &self,
        webhook: ws::Webhook,
    ) -> Result<ws::SingleResponse<ws::Webhook>, wsc::Error> {
        let Ok(mut map) = self.0.write() else {
            return Err(wsc::Error::MutexPoisoned(ws::ModelKind::Webhook));
        };

        if map.contains_key(&webhook.webhook_id) {
            return Err(wsc::Error::AlreadyExists(ws::ModelKind::Webhook));
        }

        map.insert(webhook.webhook_id.clone(), Webhook(webhook.clone()));

        Ok(ws::SingleResponse { item: webhook })
    }

    pub async fn get(
        &self,
        id: ws::WebhookId,
    ) -> Result<ws::SingleResponse<ws::Webhook>, wsc::Error> {
        let Ok(map) = self.0.read() else {
            return Err(wsc::Error::MutexPoisoned(ws::ModelKind::Webhook));
        };

        let webhook = map.get(&id);
        let webhook = match webhook {
            Some(webhook) => webhook.clone(),
            None => return Err(wsc::Error::NotFound(ws::ModelKind::Webhook)),
        };

        Ok(ws::SingleResponse { item: webhook.0 })
    }

    pub async fn delete(&self, id: ws::WebhookId) -> Result<(), wsc::Error> {
        let Ok(mut map) = self.0.write() else {
            return Err(wsc::Error::MutexPoisoned(ws::ModelKind::Webhook));
        };

        let item = map.remove(&id);
        match item {
            Some(_) => Ok(()),
            None => Err(wsc::Error::NotFound(ws::ModelKind::Webhook)),
        }
    }
}
