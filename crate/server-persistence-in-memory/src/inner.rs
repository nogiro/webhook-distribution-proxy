use wdp_server_core as wsc;
use wdp_struct as ws;

mod webhook;

use async_trait::async_trait;

#[derive(Default)]
pub struct PersistenceInMemory {
    webhook: webhook::WebhookContainer,
}

#[async_trait]
impl wsc::PersistenceTrait for PersistenceInMemory {
    async fn list_webhook(
        &self,
        pagination: ws::Pagination,
    ) -> Result<ws::ListResponse<ws::Webhook>, wsc::Error> {
        self.webhook.list(pagination).await
    }

    async fn add_webhook(
        &self,
        webhook: ws::Webhook,
    ) -> Result<ws::SingleResponse<ws::Webhook>, wsc::Error> {
        self.webhook.add(webhook).await
    }

    async fn get_webhook(
        &self,
        id: ws::WebhookId,
    ) -> Result<ws::SingleResponse<ws::Webhook>, wsc::Error> {
        self.webhook.get(id).await
    }

    async fn delete_webhook(&self, id: ws::WebhookId) -> Result<(), wsc::Error> {
        self.webhook.delete(id).await
    }
}
