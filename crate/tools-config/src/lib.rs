mod args;
mod error;
mod generate;

pub use args::Args;
pub use error::Error;

pub async fn run(args: Args) -> Result<(), Error> {
    println!("args: {args:?}");
    match args.action {
        args::Action::Generate(args) => generate::run(args).await,
    }
}

#[cfg(test)]
mod tests {}
