use wdp_server_config as config;

use crate::Error;
use clap::Parser;
use std::path::Path;
use tokio::fs::{create_dir_all, write};

#[derive(Parser, Debug)]
pub struct GenerateArgs {
    #[arg(short, long, default_value_t = config::DEFAULT_PATH.to_string())]
    pub file: String,

    #[arg(long)]
    pub force: bool,
}

pub async fn run(args: GenerateArgs) -> Result<(), Error> {
    let config = config::Config::default();
    let s = toml::to_string(&config)?;

    let path = Path::new(&args.file);
    if path.exists() && !args.force {
        return Err(Error::AlreadyExists(args.file));
    }

    if let Some(dir) = path.parent() {
        create_dir_all(dir).await?;
    }

    write(path, s).await?;

    Ok(())
}
