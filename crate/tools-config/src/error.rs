#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("error in I/O: {0}")]
    Io(#[from] std::io::Error),

    #[error("error in serializing TOML: {0}")]
    SerToml(#[from] toml::ser::Error),

    #[error("file already exists: {0}\n  If you want to overwrite, specify the `--force` option.")]
    AlreadyExists(String),
}
