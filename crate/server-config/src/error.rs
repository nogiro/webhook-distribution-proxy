#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("error in I/O: {0}")]
    Io(#[from] std::io::Error),

    #[error("failed to deserialize TOML : {0}")]
    TomlDeserializing(#[from] toml::de::Error),
}
