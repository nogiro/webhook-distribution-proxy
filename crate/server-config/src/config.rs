use wdp_server_http as wsh;

mod http;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug, Default)]
pub struct Config {
    pub http: http::Config,
}

impl Config {
    pub fn http(&self) -> wsh::Config {
        self.http.clone().into()
    }
}
