use wdp_server_http as wsh;

use serde::{Deserialize, Serialize};
use std::net::SocketAddr;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Config {
    host: SocketAddr,
}

impl From<Config> for wsh::Config {
    fn from(config: Config) -> Self {
        Self { host: config.host }
    }
}

impl Default for Config {
    fn default() -> Self {
        let host = "127.0.0.1:3000".parse().unwrap();
        Self { host }
    }
}
