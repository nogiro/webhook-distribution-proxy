mod config;
mod error;

pub use config::Config;
pub use error::Error;

use tokio::fs::read_to_string;

pub const DEFAULT_PATH: &str = ".wdp/config.toml";

pub async fn load(path: String) -> Result<Config, Error> {
    let s = read_to_string(path).await?;
    load2(&s)
}

fn load2(s: &str) -> Result<Config, Error> {
    toml::from_str(s).map_err(Error::from)
}

#[cfg(test)]
mod tests {}
