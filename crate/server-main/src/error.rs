use wdp_server_config as config;
use wdp_server_http as wsh;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("error in http layer: {0}")]
    Http(#[from] wsh::Error),

    #[error("error in config layer: {0}")]
    Config(#[from] config::Error),
}
