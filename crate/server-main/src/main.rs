use wdp_server_call as call;
use wdp_server_config as config;
use wdp_server_core as wsc;
use wdp_server_http as wsh;
use wdp_server_persistence_in_memory as wspim;

mod error;
mod logger;

use error::Error;

#[tokio::main]
async fn main() -> Result<(), Error> {
    logger::init();

    let config = config::load(".wdp/config.toml".to_string()).await?;
    let call = wsc::CallAdapter::new(call::Caller::default());
    let notification = wsc::NotificationAdapter::new(wsh::Notification::<()>::default());
    let persistence = wsc::PersistenceAdapter::new(wspim::PersistenceInMemory::default());

    let state = wsh::AppState::new(call, notification, persistence);
    wsh::serve(&config.http(), state).await.map_err(Error::from)
}
