use wdp_tools_config as wtc;

use clap::{Parser, Subcommand};
use tracing::Level;

#[derive(Parser, Debug)]
pub struct Args {
    #[arg(short, long, default_value_t = Level::INFO)]
    pub log_level: Level,

    #[command(subcommand)]
    pub action: Action,
}

#[derive(Subcommand, Debug)]
pub enum Action {
    Config(wtc::Args),
}
