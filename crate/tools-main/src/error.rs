use wdp_tools_config as wtc;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("error in I/O: {0}")]
    Io(#[from] std::io::Error),

    #[error("error in `config-template`: {0}")]
    Config(#[from] wtc::Error),
}
