use wdp_tools_config as wtc;

mod args;
mod error;

use args::Args;
use clap::Parser;
use error::Error;

#[tokio::main]
async fn main() {
    let args = Args::parse();

    tracing_subscriber::fmt()
        .with_max_level(args.log_level)
        .init();
    println!("args: {args:?}");

    let result = match args.action {
        args::Action::Config(args_inner) => wtc::run(args_inner).await,
    };

    println!("result: {:?}", result.map_err(Error::from));
}
