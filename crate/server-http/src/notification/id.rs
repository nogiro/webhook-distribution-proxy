use wdp_struct as ws;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct ConnectionId<T> {
    id: ws::Id<Self>,
    auth_id: T,
}
