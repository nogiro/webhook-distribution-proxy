use axum::body::HttpBody;
use axum::extract::rejection::JsonRejection;
use axum::extract::FromRequest;
use axum::http::{Request, StatusCode};
use axum::response::{IntoResponse, Response};
use axum::Json as AxJson;
use axum::{async_trait, BoxError};
use serde::de::DeserializeOwned;
use serde_json::json;

pub struct Json<T>(pub T);

#[async_trait]
impl<S, B, T> FromRequest<S, B> for Json<T>
where
    B: HttpBody + Send + 'static,
    B::Data: Send,
    B::Error: Into<BoxError>,
    S: Send + Sync,
    T: DeserializeOwned,
{
    type Rejection = Response;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        let result = AxJson::from_request(req, state).await;

        let AxJson(body) = match result {
            Ok(result) => result,
            Err(msg) => {
                let status = match &msg {
                    JsonRejection::BytesRejection(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    _ => StatusCode::BAD_REQUEST,
                };

                return Err((status, AxJson(json!({"msg": msg.to_string()}))).into_response());
            }
        };

        Ok(Self(body))
    }
}
