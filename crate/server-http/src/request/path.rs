use axum::async_trait;
use axum::extract::rejection::PathRejection;
use axum::extract::FromRequestParts;
use axum::extract::Path as AxPath;
use axum::http::request::Parts;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::Json as AxJson;
use serde::de::DeserializeOwned;
use serde_json::json;

pub struct Path<T>(pub T);

#[async_trait]
impl<S, T> FromRequestParts<S> for Path<T>
where
    S: Send + Sync,
    T: DeserializeOwned + Send,
{
    type Rejection = Response;

    async fn from_request_parts(req: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let result = AxPath::from_request_parts(req, state).await;
        let AxPath(body) = match result {
            Ok(result) => result,
            Err(msg) => {
                let status = match &msg {
                    PathRejection::FailedToDeserializePathParams(_) => StatusCode::BAD_REQUEST,
                    _ => StatusCode::INTERNAL_SERVER_ERROR,
                };
                return Err((status, AxJson(json!({"msg": msg.to_string()}))).into_response());
            }
        };

        Ok(Self(body))
    }
}
