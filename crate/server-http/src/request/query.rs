use axum::async_trait;
use axum::extract::rejection::QueryRejection;
use axum::extract::FromRequestParts;
use axum::extract::Query as AxQuery;
use axum::http::request::Parts;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::Json as AxJson;
use serde::de::DeserializeOwned;
use serde_json::json;

pub struct Query<T>(pub T);

#[async_trait]
impl<S, T> FromRequestParts<S> for Query<T>
where
    S: Send + Sync,
    T: DeserializeOwned,
{
    type Rejection = Response;

    async fn from_request_parts(req: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let result = AxQuery::from_request_parts(req, state).await;
        let AxQuery(body) = match result {
            Ok(result) => result,
            Err(msg) => {
                let status = match &msg {
                    QueryRejection::FailedToDeserializeQueryString(_) => StatusCode::BAD_REQUEST,
                    _ => StatusCode::INTERNAL_SERVER_ERROR,
                };
                return Err((status, AxJson(json!({"msg": msg.to_string()}))).into_response());
            }
        };

        Ok(Self(body))
    }
}
