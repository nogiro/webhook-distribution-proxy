use wdp_server_core as wsc;

pub struct AppState<C, N, P>(wsc::App<C, N, P>)
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait;

impl<C, N, P> AppState<C, N, P>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    pub fn new(c: C, n: N, p: P) -> Self {
        let c = wsc::CallAdapter::new(c);
        let n = wsc::NotificationAdapter::new(n);
        let p = wsc::PersistenceAdapter::new(p);
        Self(wsc::App::new(c, n, p))
    }
}

impl<C, N, P> core::ops::Deref for AppState<C, N, P>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    type Target = wsc::App<C, N, P>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
