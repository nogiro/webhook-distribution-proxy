use wdp_server_core as wsc;

mod config;
mod error;
mod notification;
mod request;
mod router;
mod state;

pub use config::Config;
pub use error::Error;
pub use notification::Notification;
pub use state::AppState;

use axum::Server;
use request::{Json, Path, Query};
use std::sync::Arc;

pub async fn serve<'a, C, N, P>(config: &'a Config, state: AppState<C, N, P>) -> Result<(), Error>
where
    C: wsc::CallTrait + 'a + 'static,
    N: wsc::NotificationTrait + 'a + 'static,
    P: wsc::PersistenceTrait + 'a + 'static,
{
    let app = router::router(Arc::new(state));

    Server::bind(&config.host)
        .serve(app.into_make_service())
        .await
        .map_err(Error::from)
}
