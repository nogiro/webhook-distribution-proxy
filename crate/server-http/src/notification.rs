use wdp_server_core as wsc;
use wdp_struct as ws;

mod id;

pub use id::ConnectionId;

use async_trait::async_trait;
use dashmap::DashMap;
use http::{Request, Response};
use hyper::body::Body;
use std::hash::Hash;
use ws::RequestId;
use wsc::NotificationTrait;

type Func = fn(RequestId, String, Request<Body>);

pub struct Notification<AUTH> {
    taps: DashMap<ConnectionId<AUTH>, Func>,
}

impl<AUTH> Default for Notification<AUTH>
where
    AUTH: Hash + Send + Sync + Eq + PartialEq,
{
    fn default() -> Self {
        Self {
            taps: Default::default(),
        }
    }
}

#[async_trait]
impl<AUTH> NotificationTrait for Notification<AUTH>
where
    AUTH: Hash + Send + Sync + Eq + PartialEq,
{
    type ID = ConnectionId<AUTH>;
    type F = Func;

    async fn add_tap(&self, connection_id: Self::ID, func: Self::F) -> Result<(), wsc::Error> {
        self.taps.insert(connection_id, func);
        Ok(())
    }

    async fn notice(
        &self,
        id: RequestId,
        path: String,
        req: Request<Body>,
    ) -> Result<Response<Body>, wsc::Error> {
        let _ = id;
        let _ = path;
        let _ = req;
        todo!()
    }
}
