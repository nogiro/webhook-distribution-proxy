use wdp_server_core as wsc;
use wdp_struct as ws;

use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::Json;
use serde_json::json;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("error in hyper: {0}")]
    Hyper(#[from] hyper::Error),

    #[error("error in core: {0}")]
    Core(#[from] wsc::Error),

    #[error("not implemented: {0:?}")]
    NotImplemented(ws::ModelKind),

    #[error("unknown error")]
    Unknown,
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        tracing::info!("response: {self}");
        match self {
            Self::NotImplemented(_) => (
                StatusCode::NOT_IMPLEMENTED,
                Json(json!({"msg": "not implemented"})),
            ),

            Self::Core(wsc::Error::NotFound(_)) => {
                (StatusCode::NOT_FOUND, Json(json!({"msg": "not found"})))
            }

            Self::Core(wsc::Error::AlreadyExists(_)) => (
                StatusCode::CONFLICT,
                Json(json!({"msg": "conflict: already exists"})),
            ),

            Self::Core(wsc::Error::UrlParseError(_, _))
            | Self::Core(wsc::Error::UrlConvertError(_, _)) => (
                StatusCode::CONFLICT,
                Json(json!({"msg": "conflict: failed to treat url"})),
            ),

            Self::Core(wsc::Error::ReqwestRequestError(_))
            | Self::Core(wsc::Error::HyperRequestError(_)) => (
                StatusCode::BAD_GATEWAY,
                Json(json!({"msg": "bad gateway: failed to request"})),
            ),

            Self::Hyper(_)
            | Self::Core(wsc::Error::MutexPoisoned(_))
            | Self::Core(wsc::Error::Unknown)
            | Self::Unknown => (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({"msg": "internal server error"})),
            ),
        }
        .into_response()
    }
}
