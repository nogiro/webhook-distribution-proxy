use wdp_server_core as wsc;

use crate::AppState;
use axum::Router;
use std::sync::Arc;

pub fn router<C, N, P>(state: Arc<AppState<C, N, P>>) -> Router
where
    C: wsc::CallTrait + 'static,
    N: wsc::NotificationTrait + 'static,
    P: wsc::PersistenceTrait + 'static,
{
    Router::new().with_state(state)
}
