use wdp_server_core as wsc;
use wdp_struct as ws;

use crate::{AppState, Error, Json, Path, Query};
use axum::body::Body;
use axum::extract::ws::{WebSocket, WebSocketUpgrade};
use axum::extract::State;
use axum::http::Request;
use axum::response::IntoResponse;
use axum::routing::{any, get};
use axum::Json as AxJson;
use axum::Router;
use std::sync::Arc;
use tracing::instrument;

pub fn router<C, N, P>(state: Arc<AppState<C, N, P>>) -> Router
where
    C: wsc::CallTrait + 'static,
    N: wsc::NotificationTrait + 'static,
    P: wsc::PersistenceTrait + 'static,
{
    Router::new()
        .route(
            "/",
            get(list_webhook::<C, N, P>).post(add_webhook::<C, N, P>),
        )
        .route(
            "/:id",
            get(get_webhook::<C, N, P>).delete(delete_webhook::<C, N, P>),
        )
        .route("/:id/call", any(call_without_path))
        .route("/:id/call/*path", any(call))
        .route("/:id/connect", get(connect))
        .with_state(state)
}

#[instrument(skip(state))]
async fn list_webhook<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Query(pagination): Query<ws::Pagination>,
) -> Result<AxJson<ws::ListResponse<ws::Webhook>>, Error>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    let result = wsc::Webhook::list(&state, pagination).await?;
    Ok(AxJson(result))
}

#[instrument(skip(state))]
async fn add_webhook<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Json(webhook): Json<ws::Webhook>,
) -> Result<AxJson<ws::SingleResponse<ws::Webhook>>, Error>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    let result = wsc::Webhook::add(&state, webhook).await?;
    Ok(AxJson(result))
}

#[instrument(skip(state))]
async fn get_webhook<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Path(id): Path<ws::WebhookId>,
) -> Result<AxJson<ws::SingleResponse<ws::Webhook>>, Error>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    let result = wsc::Webhook::get(&state, id).await?;
    Ok(AxJson(result))
}

#[instrument(skip(state))]
async fn delete_webhook<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Path(id): Path<ws::WebhookId>,
) -> Result<AxJson<ws::SingleResponse<()>>, Error>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    wsc::Webhook::delete(&state, id).await?;
    Ok(AxJson(ws::SingleResponse::<_> { item: () }))
}

#[instrument(skip(state))]
async fn call_without_path<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Path(id): Path<ws::WebhookId>,
    req: Request<Body>,
) -> Result<impl IntoResponse, Error>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    call(State(state), Path((id, "/".to_string())), req).await
}

#[instrument(skip(state))]
async fn call<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Path((id, path)): Path<(ws::WebhookId, String)>,
    req: Request<Body>,
) -> Result<impl IntoResponse, Error>
where
    C: wsc::CallTrait,
    N: wsc::NotificationTrait,
    P: wsc::PersistenceTrait,
{
    let resp = wsc::Webhook::call(&state, id, path, req).await?;
    Ok(resp)
}

#[instrument(skip(state))]
async fn connect<C, N, P>(
    State(state): State<Arc<AppState<C, N, P>>>,
    Path(id): Path<ws::WebhookId>,
    ws: WebSocketUpgrade,
) -> Result<impl IntoResponse, Error>
where
    C: wsc::CallTrait + 'static,
    N: wsc::NotificationTrait + 'static,
    P: wsc::PersistenceTrait + 'static,
{
    let result = wsc::Webhook::get(&state, id).await?.item;

    let resp = ws.on_upgrade(move |socket| handle_connect(state, socket, result));
    Ok(resp)
}

async fn handle_connect<C, N, P>(
    state: Arc<AppState<C, N, P>>,
    socket: WebSocket,
    result: ws::Webhook,
) where
    C: wsc::CallTrait + 'static,
    N: wsc::NotificationTrait + 'static,
    P: wsc::PersistenceTrait + 'static,
{
    let _ = state;
    let _ = socket;
    let _ = result;
}
