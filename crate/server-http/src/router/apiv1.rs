use wdp_server_core as wsc;

mod login;
mod responses;
mod webhooks;

use crate::AppState;
use axum::Router;
use std::sync::Arc;

pub fn router<C, N, P>(state: Arc<AppState<C, N, P>>) -> Router
where
    C: wsc::CallTrait + 'static,
    N: wsc::NotificationTrait + 'static,
    P: wsc::PersistenceTrait + 'static,
{
    Router::new()
        .nest("/login", login::router::<C, N, P>(state.clone()))
        .nest("/responses", responses::router::<C, N, P>(state.clone()))
        .nest("/webhooks", webhooks::router::<C, N, P>(state))
}
