use std::net::SocketAddr;

#[derive(Clone, Debug)]
pub struct Config {
    pub host: SocketAddr,
}
