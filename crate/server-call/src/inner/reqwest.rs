use wdp_server_core as wsc;

use http::{Request, Response};
use hyper::body::Body;
use reqwest::Client;
use url::Url;

pub async fn call(url: Url, req: Request<Body>) -> Result<Response<Body>, wsc::Error> {
    let req = into_req(url, req)?;

    let resp = Client::new()
        .execute(req)
        .await
        .map_err(|e| wsc::Error::ReqwestRequestError(Box::new(e)))?;

    let body = Body::wrap_stream(resp.bytes_stream());
    Ok(Response::new(body))
}

fn into_req(url: Url, req: Request<Body>) -> Result<reqwest::Request, wsc::Error> {
    let (parts, body) = req.into_parts();
    tracing::debug!("parts: {parts:?}");

    let builder = Client::new().request(parts.method, url);

    let req = builder
        .body(body)
        .build()
        .map_err(|e| wsc::Error::UrlConvertError(Box::new(e), "prepare".to_string()))?;

    Ok(req)
}
