#[cfg(not(any(feature = "reqwest", feature = "hyper")))]
compile_error!(
    r#"At least one of the feature flags ("reqwest", "hyper") must be enabled for this crate."#
);

mod inner;

pub use inner::Caller;
