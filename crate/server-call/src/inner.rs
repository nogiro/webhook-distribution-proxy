use wdp_server_core as wsc;

#[cfg(feature = "reqwest")]
mod reqwest;

use async_trait::async_trait;
use http::{Request, Response};
use hyper::body::Body;
use url::Url;

#[derive(Default)]
pub enum Caller {
    #[cfg_attr(feature = "reqwest", default)]
    #[cfg(feature = "reqwest")]
    Reqwest,

    #[cfg_attr(all(not(feature = "reqwest"), feature = "hyper"), default)]
    #[cfg(feature = "hyper")]
    Hyper,
}

#[async_trait]
impl wsc::CallTrait for Caller {
    async fn call(&self, url: Url, req: Request<Body>) -> Result<Response<Body>, wsc::Error> {
        match self {
            #[cfg(feature = "reqwest")]
            Self::Reqwest => reqwest::call(url, req).await,

            #[cfg(feature = "hyper")]
            Self::Hyper => Self::call_by_hyper(url, req).await,
        }
    }
}

impl Caller {
    #[cfg(feature = "hyper")]
    async fn call_by_hyper(url: Url, req: Request<Body>) -> Result<Response<Body>, wsc::Error> {
        fn url_into_uri(url: Url) -> Result<hyper::Uri, wsc::Error> {
            let _ = url;
            todo!()
        }

        let (mut parts, body) = req.into_parts();
        parts.uri = url_into_uri(url)?;
        let req = Request::<Body>::from_parts(parts, body);
        hyper::Client::new()
            .request(req)
            .await
            .map_err(wsc::Error::HyperRequestError)
    }
}
