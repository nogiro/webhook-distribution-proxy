use crate::time;
use crate::WebhookId;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Deserialize, Serialize, Clone, Debug, Hash, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub struct Webhook {
    #[serde(default)]
    pub webhook_id: WebhookId,

    #[serde(flatten)]
    pub kind: WebhookKind,

    #[serde(default = "time::now")]
    pub created_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Clone, Debug, Hash, PartialEq, Eq)]
#[serde(rename_all = "kebab-case", tag = "kind")]
pub enum WebhookKind {
    PassTrough { endpoint: Url },
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn de_webhook() {
        let testdata = r#"
        {
            "kind": "pass-trough",
            "endpoint": "https://endpoint.dummy"
        }
        "#;
        serde_json::from_str::<Webhook>(&testdata).unwrap();
    }
}
