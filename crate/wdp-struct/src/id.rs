use serde::de::{Deserialize, Deserializer};
use serde::ser::{Serialize, Serializer};
use std::marker::PhantomData;
use std::ops::Deref;
use uuid::Uuid;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Webhook;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Request;

pub type WebhookId = Id<Webhook>;
pub type RequestId = Id<Request>;

#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
pub struct Id<T> {
    id: Uuid,
    _phantom: PhantomData<T>,
}

impl<'de, T> Deserialize<'de> for Id<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let id = Uuid::deserialize(deserializer)?;
        Ok(Self {
            id,
            ..Default::default()
        })
    }
}

impl<T> Serialize for Id<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        Uuid::serialize(&self.id, serializer)
    }
}

impl<T> PartialOrd for Id<T>
where
    T: Eq,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl<T> Ord for Id<T>
where
    T: Eq,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.id.cmp(&other.id)
    }
}

impl<T> Id<T> {
    pub const fn new(id: Uuid) -> Self {
        Self {
            id,
            _phantom: PhantomData,
        }
    }
}

impl<T> Default for Id<T> {
    fn default() -> Self {
        Self::new(Uuid::new_v4())
    }
}

impl<T> AsRef<Uuid> for Id<T> {
    fn as_ref(&self) -> &Uuid {
        &self.id
    }
}

impl<T> Deref for Id<T> {
    type Target = Uuid;

    fn deref(&self) -> &Self::Target {
        &self.id
    }
}
