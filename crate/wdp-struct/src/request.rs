use serde::{Deserialize, Serialize};
use std::cmp::max;

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Pagination {
    #[serde(default = "default_offset")]
    offset: i64,
    #[serde(default = "default_limit")]
    limit: i64,
}

const fn default_offset() -> i64 {
    0
}

const fn default_limit() -> i64 {
    10
}

impl Pagination {
    pub fn pagination(&self) -> (i64, i64) {
        let offset = max(0, self.offset);
        let limit = self.limit.clamp(0, 50);

        (offset, limit)
    }
}
