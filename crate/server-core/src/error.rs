use wdp_struct as ws;

type BoxError = Box<dyn std::error::Error + Send + Sync>;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("error in mutex: {0:?}")]
    MutexPoisoned(ws::ModelKind),

    #[error("already exists: {0:?}")]
    AlreadyExists(ws::ModelKind),

    #[error("not found: {0:?}")]
    NotFound(ws::ModelKind),

    #[error("error in parse url: {0:?}, {1}")]
    UrlParseError(url::ParseError, String),

    #[error("error in convert url into uri: {0:?}, {1}")]
    UrlConvertError(BoxError, String),

    #[error("request error by reqwest: {0}")]
    ReqwestRequestError(BoxError),

    #[error("request error by hyper: {0}")]
    HyperRequestError(hyper::Error),

    #[error("unknown error")]
    Unknown,
}
