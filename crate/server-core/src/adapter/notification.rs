use wdp_struct as ws;

use crate::Error;
use async_trait::async_trait;
use http::{Request, Response};
use hyper::body::Body;
use std::hash::Hash;
use ws::RequestId;

#[cfg(test)]
use mockall::automock;

pub struct NotificationAdapter<T>(T)
where
    T: NotificationTrait;

impl<T> NotificationAdapter<T>
where
    T: NotificationTrait,
{
    pub const fn new(notification: T) -> Self {
        Self(notification)
    }
}

#[cfg_attr(test, automock(type ID=(); type F=fn(RequestId, String, Request<Body>);))]
#[async_trait]
pub trait NotificationTrait: Send + Sync {
    type ID: Hash + Send + Sync + Eq + PartialEq;
    type F: Fn(RequestId, String, Request<Body>) + Send + Sync;

    async fn add_tap(&self, connection_id: Self::ID, func: Self::F) -> Result<(), Error>;

    async fn notice(
        &self,
        id: RequestId,
        path: String,
        req: Request<Body>,
    ) -> Result<Response<Body>, Error>;
}

#[async_trait]
impl<T> NotificationTrait for NotificationAdapter<T>
where
    T: NotificationTrait + Send + Sync,
{
    type ID = T::ID;
    type F = T::F;

    async fn add_tap(&self, connection_id: Self::ID, func: Self::F) -> Result<(), Error> {
        self.0.add_tap(connection_id, func).await
    }

    async fn notice(
        &self,
        id: RequestId,
        path: String,
        req: Request<Body>,
    ) -> Result<Response<Body>, Error> {
        self.0.notice(id, path, req).await
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    pub fn mock() -> NotificationAdapter<MockNotificationTrait> {
        let mut mock = MockNotificationTrait::new();
        mock.expect_add_tap().returning(|_, _| Ok(()));
        mock.expect_notice()
            .returning(|_, _, _| Ok(Response::new(Body::empty())));

        NotificationAdapter::new(mock)
    }

    #[test]
    fn create_mock() {
        mock();
    }
}
