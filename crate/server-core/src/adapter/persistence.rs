use wdp_struct as ws;

use crate::Error;
use async_trait::async_trait;

#[cfg(test)]
use mockall::automock;

pub struct PersistenceAdapter<T>(T)
where
    T: PersistenceTrait;

impl<T> PersistenceAdapter<T>
where
    T: PersistenceTrait,
{
    pub const fn new(persistence: T) -> Self {
        Self(persistence)
    }
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait PersistenceTrait: Send + Sync {
    async fn list_webhook(
        &self,
        pagination: ws::Pagination,
    ) -> Result<ws::ListResponse<ws::Webhook>, Error>;

    async fn add_webhook(
        &self,
        webhook: ws::Webhook,
    ) -> Result<ws::SingleResponse<ws::Webhook>, Error>;

    async fn get_webhook(
        &self,
        id: ws::WebhookId,
    ) -> Result<ws::SingleResponse<ws::Webhook>, Error>;

    async fn delete_webhook(&self, id: ws::WebhookId) -> Result<(), Error>;
}

#[async_trait]
impl<T> PersistenceTrait for PersistenceAdapter<T>
where
    T: PersistenceTrait + Send + Sync,
{
    async fn list_webhook(
        &self,
        pagination: ws::Pagination,
    ) -> Result<ws::ListResponse<ws::Webhook>, Error> {
        self.0.list_webhook(pagination).await
    }

    async fn add_webhook(
        &self,
        webhook: ws::Webhook,
    ) -> Result<ws::SingleResponse<ws::Webhook>, Error> {
        self.0.add_webhook(webhook).await
    }

    async fn get_webhook(
        &self,
        id: ws::WebhookId,
    ) -> Result<ws::SingleResponse<ws::Webhook>, Error> {
        self.0.get_webhook(id).await
    }

    async fn delete_webhook(&self, id: ws::WebhookId) -> Result<(), Error> {
        self.0.delete_webhook(id).await
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn dummy_webhook() -> ws::Webhook {
        ws::Webhook {
            webhook_id: Default::default(),
            kind: ws::WebhookKind::PassTrough {
                endpoint: "https://endpoint.dummy".parse().unwrap(),
            },
            created_at: "2023-01-01T12:12:12".parse().unwrap(),
        }
    }

    fn wrap_single<T>(item: T) -> Result<ws::SingleResponse<T>, Error> {
        Ok(ws::SingleResponse { item })
    }

    fn wrap_list<T>(
        items: Vec<T>,
        offset: i64,
        limit: i64,
        count: i64,
    ) -> Result<ws::ListResponse<T>, Error> {
        Ok(ws::ListResponse {
            items,
            offset,
            limit,
            count,
        })
    }

    pub fn mock() -> PersistenceAdapter<MockPersistenceTrait> {
        let mut mock = MockPersistenceTrait::new();
        mock.expect_list_webhook()
            .returning(|_| wrap_list(vec![], 0, 0, 0));
        mock.expect_add_webhook()
            .returning(|_| wrap_single(dummy_webhook()));
        mock.expect_get_webhook()
            .returning(|_| wrap_single(dummy_webhook()));
        mock.expect_delete_webhook().returning(|_| Ok(()));

        PersistenceAdapter::new(mock)
    }

    #[test]
    fn create_mock() {
        mock();
    }
}
