use crate::Error;
use async_trait::async_trait;
use http::{Request, Response};
use hyper::body::Body;
use url::Url;

#[cfg(test)]
use mockall::automock;

pub struct CallAdapter<T>(T)
where
    T: CallTrait;

impl<T> CallAdapter<T>
where
    T: CallTrait,
{
    pub const fn new(call: T) -> Self {
        Self(call)
    }
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait CallTrait: Send + Sync {
    async fn call(&self, url: Url, req: Request<Body>) -> Result<Response<Body>, Error>;
}

#[async_trait]
impl<T> CallTrait for CallAdapter<T>
where
    T: CallTrait + Send + Sync,
{
    async fn call(&self, url: Url, req: Request<Body>) -> Result<Response<Body>, Error> {
        self.0.call(url, req).await
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    pub fn mock() -> CallAdapter<MockCallTrait> {
        let mut mock = MockCallTrait::new();
        mock.expect_call()
            .returning(|_, _| Ok(Response::new(Body::empty())));

        CallAdapter::new(mock)
    }

    #[test]
    fn create_mock() {
        mock();
    }
}
