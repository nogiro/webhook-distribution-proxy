use wdp_struct as ws;

use crate::Error;
use crate::{App, CallTrait, NotificationTrait, PersistenceTrait};
use http::{Request, Response};
use hyper::body::Body;
use std::marker::PhantomData;

pub struct Webhook<C, N, P>(ws::Webhook, PhantomData<C>, PhantomData<N>, PhantomData<P>)
where
    C: CallTrait,
    N: NotificationTrait,
    P: PersistenceTrait;

impl<C, N, P> Webhook<C, N, P>
where
    C: CallTrait,
    N: NotificationTrait,
    P: PersistenceTrait,
{
    pub async fn list(
        state: &App<C, N, P>,
        pagination: ws::Pagination,
    ) -> Result<ws::ListResponse<ws::Webhook>, Error> {
        state.persistence_ref().list_webhook(pagination).await
    }

    pub async fn add(
        state: &App<C, N, P>,
        webhook: ws::Webhook,
    ) -> Result<ws::SingleResponse<ws::Webhook>, Error> {
        state.persistence_ref().add_webhook(webhook).await
    }

    pub async fn get(
        state: &App<C, N, P>,
        id: ws::WebhookId,
    ) -> Result<ws::SingleResponse<ws::Webhook>, Error> {
        state.persistence_ref().get_webhook(id).await
    }

    pub async fn delete(state: &App<C, N, P>, id: ws::WebhookId) -> Result<(), Error> {
        state.persistence_ref().delete_webhook(id).await
    }

    pub async fn call(
        state: &App<C, N, P>,
        id: ws::WebhookId,
        path: String,
        req: Request<Body>,
    ) -> Result<Response<Body>, Error> {
        let webhook = state.persistence_ref().get_webhook(id).await?;
        match webhook.item.kind {
            ws::WebhookKind::PassTrough { endpoint } => {
                let url = endpoint
                    .join(&path)
                    .map_err(|e| Error::UrlParseError(e, path))?;
                state.call_ref().call(url, req).await
            }
        }
    }
}
