use crate::{CallAdapter, CallTrait};
use crate::{NotificationAdapter, NotificationTrait};
use crate::{PersistenceAdapter, PersistenceTrait};
use std::sync::Arc;

pub struct App<C, N, P>
where
    C: CallTrait,
    N: NotificationTrait,
    P: PersistenceTrait,
{
    call: Arc<CallAdapter<C>>,
    notification: Arc<NotificationAdapter<N>>,
    persistence: Arc<PersistenceAdapter<P>>,
}

impl<C, N, P> App<C, N, P>
where
    C: CallTrait,
    N: NotificationTrait,
    P: PersistenceTrait,
{
    pub fn new(
        call: CallAdapter<C>,
        notification: NotificationAdapter<N>,
        persistence: PersistenceAdapter<P>,
    ) -> Self {
        Self {
            call: Arc::new(call),
            notification: Arc::new(notification),
            persistence: Arc::new(persistence),
        }
    }

    pub fn call_ref(&self) -> &CallAdapter<C> {
        &self.call
    }

    pub fn call(&self) -> Arc<CallAdapter<C>> {
        self.call.clone()
    }

    pub fn notification_ref(&self) -> &NotificationAdapter<N> {
        &self.notification
    }

    pub fn notification(&self) -> Arc<NotificationAdapter<N>> {
        self.notification.clone()
    }

    pub fn persistence_ref(&self) -> &PersistenceAdapter<P> {
        &self.persistence
    }

    pub fn persistence(&self) -> Arc<PersistenceAdapter<P>> {
        self.persistence.clone()
    }
}
