# Webhook Distribution Proxy

Concept: [./docs/spec/concept.md](./docs/spec/concept.md)

## Getting Started (Future Works 1)

```sh
# install Rust toolchain
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# install cargo-make
cargo install --force cargo-make

# clone this repos
git clone git@gitlab.com:nogiro/webhook-distribution-proxy.git

# deploy 
cd webhook-distribution-proxy
cargo make tools generate-default-settings
cargo make deploy-heroku

# create endpoint
cargo make create-endpoint

# connect
cargo make connect "[host to distribute]"
```


## Customize (Future Works 1)

Copy `.wdp/default/config.yaml` to `.wdp/config.yaml`, then custom it.


## HTTP specs

Build by the following command, then visit `localhost:3000`.
```sh
docker compose -f container/docs-spec/docker-compose.yml up --build
```
